#ifndef MODELER_GLOBAL_H
#define MODELER_GLOBAL_H

#include "definition_global.h"
#include "software_global.h"
#include "opengl_global.h"

namespace paysages {
namespace modeler {
class MainModelerWindow;
class OpenGLView;

class BaseModelerTool;

class AtmosphereModeler;
class WaterModeler;

class RenderPreviewProvider;
class RenderProcess;

class ModelerCameras;

class FloatPropertyBind;
class IntPropertyBind;
}
}

using namespace paysages::modeler;

#endif // MODELER_GLOBAL_H
